Sure! Here are a few long-running TV shows that have occasional episodic cross-dressing:

1. M*A*S*H (1972-1983) - The show features several episodes where characters cross-dress for various reasons, often for comedic effect.

2. Friends (1994-2004) - In a few episodes, some of the male characters dress in women's clothing as part of their comedic antics or to fulfill certain plotlines.

3. The X-Files (1993-2018) - There are a couple of episodes where Mulder and Scully go undercover and disguise themselves by cross-dressing.

4. Farscape (1999-2003) - This sci-fi series occasionally features characters cross-dressing as part of their disguises or to navigate certain situations.

5. Buffy the Vampire Slayer (1997-2003) - In one episode titled "Halloween," the main characters unintentionally transform into their chosen Halloween costumes, resulting in some instances of cross-dressing.

6. It's Always Sunny in Philadelphia (2005-present) - This comedy series has had multiple episodes where the main characters engage in cross-dressing for comedic purposes or to achieve specific goals.

Please note that while these shows may have occasional instances of episodic cross-dressing, it is important to approach these portrayals with sensitivity and respect towards gender identity and expression.
# Sun 30 Jun 22:57:23 CEST 2024 - there's a lot of episodic cross-dressing in quantum leap. can you list any other long-running tv-shows which have occasional episodic cross-dressing?